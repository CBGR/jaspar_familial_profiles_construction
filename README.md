# JASPAR archetype computation pipeline

This repository contains the scripts used to compute the archetypes for all JASPAR profiles from the CORE collection.
Given a set of clustered JASPAR profiles, the pipeline will use the approach described in [Vierstra et al. 2020](https://doi.org/10.1038/s41586-020-2528-x) to compute the archetypal motifs.

## Downloading the pipeline

The source code to run this pipeline can be downloaded with:

```
git clone https://bitbucket.org/CBGR/jaspar_archetype_construction.git
```

## Input files and formats:

The pipeline requires the following inputs:

* **JASPAR matrices in Transfac format:** a file containing all taxon-specific JASPAR matrices in one Transfac formatted file (see [here](http://jaspar.genereg.net/download/CORE/JASPAR2020_CORE_vertebrates_non-redundant_pfms_transfac.txt) for an example). The file is expected to be stored under a path like `DATA_DIR/motifs/{taxon}/motifs.tf`, where `DATA_DIR` is the path to the directory containing the data and `taxon` is any of the taxons in JASPAR.
* **JASPAR PFM - cluster mapping:** the results from clustering the taxon-specific JASPAR matrices with the [RSAT matrix-clustering tool](http://rsat.sb-roscoff.fr/matrix-clustering_form.cgi). The expected format is a two column tab-separated .txt file named `clusters.txt` stored under a path like `DATA_DIR/clusters/{taxon}/clusters.txt`, where `DATA_DIR` is the path to the directory containing the data and `taxon` is any of the taxons in JASPAR. The first row contains the column names, which are expected to be `motifs` and `cluster`. The `motifs` column contains the matrix ID (e.g. MA0007.3), while the `cluster` column contains the cluster number each motif corresponds to.
* **Pairwise matrix comparisons:** also part of the results from clustering the taxon-specific JASPAR matrices with the [RSAT matrix-clustering tool](http://rsat.sb-roscoff.fr/matrix-clustering_form.cgi). In this case, this is a tab-separated table with the information regarding the comparison between two matrices. The file is expected to be stored at `DATA_DIR/pairwise_comparisons/{taxon}/pairwise_compa.txt`,  where `DATA_DIR` is the path to the directory containing the data and `taxon` is any of the taxons in JASPAR.  

:warning: **Make sure there is a match between the IDs used in the clusters.txt file and in the id1 and id2 columns of the pairwise comparison table!**  
:warning: **Typically, the first column in the pairwise comparison table is named "#id1". Make sure to change this to "id1"!**  
:warning: **Typically, the first and last rows of the pairwise comparison table need to be removed!**  

## Configuration of the config.yaml file

The config.yaml file contains some information regarding the paths to your input and output directories, and the taxons you want to analyze.

The variables to set there are:

* **DATA_DIR:** the path to the directory containing the input data for the pipeline.
* **RESULTS_DIR:** the path to the desired output directory.
* **TAXONS:** a list with the taxons to analyze.

## Dependencies

The pipeline has the following dependencies:

* **Python 3:** the last tested version is python 3.6.12.
    * Pandas 0.25.0
    * Numpy 1.17.0.
    * Scipy 1.3.0.
    * Re 2.2.1.
    * Matplotlib 3.1.1.
    * Seaborn 0.9.0.
* **Snakemake:** the last tested version is 5.5.4.
* **R:** the last tested version is R 4.0.2.
    * Optparse 1.6.6.
    * Universalmotif 1.10.1.

## Running the pipeline

The first step in running the pipeline is preparing the input data in the format specified in the `Input files and formats` section. Once having done that, make sure you specify the paths to the input and output directories in the `config.yaml` file (see section `Configuration of the config.yaml`). After doing this, the pipeline can be run with:
```
snakemake --cores CORES
```
where `CORES` is the number of cores to use.
