##################
## Load modules ##
##################

import sys
import os.path
import pandas as pd
import numpy as np
import scipy as sp
import re

##################################
## Definition of some functions ##
##################################

# Function to read the input meme file
def read_meme_file(filepath):
	pwms = {}
	data = open(filepath).read()
	pos = 0
	while(1):
		rec_loc = data.find('\nMOTIF', pos)
		if rec_loc < 0:
			break
		nl = data.find('\n', rec_loc + 1)
		motif = data[rec_loc+6:nl].strip()
		mat_header_start = data.find('letter-probability', rec_loc)
		mat_header_end = data.find('\n', mat_header_start + 1)
		i = data.find(':', mat_header_start)
		kp = data[i+1:mat_header_end]
		r = re.compile(r"(\w+)=\s?([\w.\+]+)\s?")
		attribs = dict(r.findall(kp))
		ist = mat_header_end + 1
		iend=-1
		j = 0
		z = []
		while(j < int(attribs["w"])):
			iend = data.find('\n', ist)
			z.append(list(map(float, data[ist:iend].strip().split())))
			ist = iend + 1
			j += 1
		pwms[motif] = np.array(z)
		pos = ist
	return pwms

# Function to compute the relative information content of a PWM
def relative_info_content(pwm):
	p = pwm/np.sum(pwm, axis = 1)[:,np.newaxis]
	ic = 2 + np.sum(p * np.nan_to_num(np.log2(p)), axis = 1)
	ric = p * ic[:,np.newaxis]
	return ric

# Function to get the reverse complement of a PWM
def reverse_complement_pwm(pwm):
	baseorder = [3,2,1,0]
	return pwm[::-1,baseorder]

# Function to compute the width of a PWM
# def pwm_widths(pwms, ids):
# 	return [pwms[i].shape[0] for i in ids]

######################
## Argument parsing ##
######################

sim_file = sys.argv[1]
cluster_file = sys.argv[2]
outdir = sys.argv[3]
meme_file = sys.argv[4]

######################
## Read input files ##
######################

sim = pd.read_csv(sim_file, header = 0, delimiter = "\t")
clusters = pd.read_csv(cluster_file, header = 0, delimiter = "\t", index_col = 0)
pwms = read_meme_file(meme_file)

##########################
## Process each cluster ##
##########################

cluster_ids = set(clusters["cluster"])

for cl in cluster_ids:

	##########################################
	# Computation of the cluster-motifs file #
	##########################################

	# Subset the relevant rows from the pairwise comparison matrix
	cl_motifs = clusters.index[clusters["cluster"] == cl]
	for subset_test in range(len(cl_motifs)):
		cl_seed_motif = cl_motifs[subset_test]
		i = (sim["id1"] == cl_seed_motif) & (sim["id2"].isin(cl_motifs))
		cl_motifs_info = sim.loc[i]
		if len(cl_motifs_info) == len(cl_motifs):
			break

	# Get information for the alignment of each motif in the cluster

	## Extract each motif's width
	cl_motifs_info["w"] = cl_motifs_info["w2"]
	## Define left side of the alignment
	left = np.min(cl_motifs_info["offset"])
	## Define each motif's offset with respect to left
	cl_motifs_info["loffset"] = -(left - cl_motifs_info["offset"])

	## Get the right side of the alignment
	right = np.max(cl_motifs_info["loffset"] + cl_motifs_info["w"])
	## Define each motif's offset with respect to right
	cl_motifs_info["roffset"] = right - cl_motifs_info["w"] - cl_motifs_info["loffset"]
	## Add a column with the cluster number information
	cl_motifs_info["cluster"] = cl

	## Keep only relevant columns
	cl_motifs_info.drop(["id1", "name1", "cor", "Ncor", "Ncor1", "Ncor2", "w1", "w2", "W", "Wr", "wr1", "wr2", "offset", "uncounted"], axis = 1, inplace = True)

	# Add strand information to know if it is necessary to reverse complement
	strand = []
	for st in cl_motifs_info["strand"]:
		if st == "D":
			strand.append("+")
		else:
			strand.append("-")

	cl_motifs_info["strand"] = strand

	# Save table
	cl_motifs_info.to_csv(os.path.join(outdir, "cluster-motifs.%d.txt" % cl), sep = "\t", header = True, index = False)

	########################################
	# Computation of the cluster-info file #
	########################################

	# Get number of motifs in the cluster being processed
	N = cl_motifs_info.shape[0]

	# Initialize a numpy array filled with zeros with the total length of the alignment
	total_ic = np.zeros(right)

	# For each motif in the cluster, get the relative information content and add it to the
	# correspondent positions of the numpy array containing the total IC
	for i in range(N):

		m = cl_motifs_info.iloc[i,:]

		pwm = pwms[m["id2"]]
		w = m["w"]
		offset = m["loffset"]
		strand = m["strand"]

		ic = relative_info_content(pwm) if strand == "+" else relative_info_content(reverse_complement_pwm(pwm))
		total_ic[offset:offset+w] += np.sum(ic, axis = 1)


	# Compute archetype borders as the positions where 95% of the total IC is found
	cdf = np.cumsum(total_ic)/np.sum(total_ic)
	s = np.where(cdf > 0.05)[0][0]
	e = np.where(cdf > 0.95)[0][0] + 1

	# Save table

	with open(os.path.join(outdir, "cluster-info.%d.txt" % cl), "w") as file:
		file.write('\t'.join(map(str, [cl, cl_seed_motif, right, s, e, N]))+'\n')
