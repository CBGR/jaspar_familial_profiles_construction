#!/bin/bash

DIR=$( realpath "$( dirname "$0" )" )

JASPAR_dir=$1
output_dir=$2

orgs=$(find $JASPAR_dir -maxdepth 1 -mindepth 1 -type d)

for org in ${orgs[@]}
do

  # mkdir -p $output_dir/$(basename $org)/tomtom/

  meme_file=$(find $org -name "aggregated_meme.meme")
  # tomtom -dist kullback -text -min-overlap 1 -motif-pseudo 0.1 -thresh 1 $meme_file $meme_file > $output_dir/$(basename $org)/tomtom/tomtom.all.txt

  python $DIR/hierarchical.py $output_dir/$(basename $org)/tomtom/tomtom.all.txt $output_dir/$(basename $org)/tomtom/ $org/families.tsv

  mkdir -p $output_dir/$(basename $org)/tomtom/processed_clusters

  cl_file=$(find $output_dir/$(basename $org)/tomtom/ -name "clusters*.txt")

  dname=$(basename $cl_file)
  dname=${dname//"clusters"/"height"}
  dname=${dname//".txt"/""}

  mkdir -p $output_dir/$(basename $org)/tomtom/processed_clusters/$dname

  python $DIR/process_cluster.py $output_dir/$(basename $org)/tomtom/tomtom.all.txt $cl_file $output_dir/$(basename $org)/tomtom/processed_clusters/$dname/ $meme_file

  mkdir -p $output_dir/$(basename $org)/tomtom/processed_clusters/$dname/plots

  clusters=($(find $output_dir/$(basename $org)/tomtom/processed_clusters/$dname/ -name "cluster-info*.txt"))

  for cl in ${clusters[@]}
  do

    cl_motifs=${cl//"info"/"motifs"}
    outfile=$(basename $cl)
    outfile=${outfile//"cluster-info."/""}
    outfile=${outfile//".txt"/""}
    outfile=$output_dir/$(basename $org)/tomtom/processed_clusters/$dname/plots/cluster${outfile}

    python $DIR/build_archetype_per_cluster.py $cl $cl_motifs $outfile $org/
    python $DIR/viz_cluster.py $cl $cl_motifs $outfile $org/

  done

done
