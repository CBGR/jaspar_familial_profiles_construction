##################
## Load modules ##
##################

import sys
import os.path
import pandas as pd
import numpy as np
import scipy as sp
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import rcParams
rcParams['pdf.fonttype'] = 42
rcParams['svg.fonttype'] = 'none'
from genome_tools.plotting import pwm
import re

##################################
## Definition of some functions ##
##################################

# Function to read the input meme file
def read_meme_file(filepath):
	data = open(filepath).read()
	pos = 0
	while(1):
		rec_loc = data.find('\nMOTIF', pos)
		if rec_loc < 0:
			break
		nl = data.find('\n', rec_loc + 1)
		motif = data[rec_loc+6:nl].strip()
		mat_header_start = data.find('letter-probability', rec_loc)
		mat_header_end = data.find('\n', mat_header_start + 1)
		i = data.find(':', mat_header_start)
		kp = data[i+1:mat_header_end]
		r = re.compile(r"(\w+)=\s?([\w.\+]+)\s?")
		attribs = dict(r.findall(kp))
		ist = mat_header_end + 1
		iend=-1

		j = 0
		z = []
		while(j < int(attribs["w"])):
			iend = data.find('\n', ist)
			z.append(list(map(float, data[ist:iend].strip().split())))
			ist = iend + 1
			j += 1

		pwm = pd.DataFrame(list(map(np.ravel, z)))
		pwm = pwm.transpose()
		pwm = np.array(pwm)

		pos = ist

	return pwm

######################
## Argument parsing ##
######################

cl_info_file = sys.argv[1]
cl_motifs_file = sys.argv[2]
outfile = sys.argv[3]
inputdir = sys.argv[4]

######################
## Read input files ##
######################

cluster_info = pd.read_csv(cl_info_file, delimiter = "\t", header = None)
cluster_info.columns = ["cl", "seed_motif", "roffset", "s", "e", "N"]

cl_motifs_info = pd.read_csv(cl_motifs_file,  delimiter = "\t", header = 0)

###################################################
## Plot the aligned motifs for the input cluster ##
###################################################

N = cl_motifs_info.shape[0]
right = np.max(cl_motifs_info["loffset"] + cl_motifs_info["w"])

s = cluster_info["s"][0]
e = cluster_info["e"][0]

padding = 2

fig = plt.figure()

width = (right + padding*2)*0.15
label_size = 2.0

fig.set_size_inches(width + label_size, N*0.35)

gs = gridspec.GridSpec(N, 2, wspace = 0, width_ratios = [1, label_size/width])

plt.subplots_adjust(left = 0, top = 1, bottom = 0)

for i in range(N):
    ax = fig.add_subplot(gs[i,0])
    m = cl_motifs_info.iloc[i,:]
    mat_id = m["id2"]
    # mat_id = mat_id[0]
    mat_id = mat_id + ".meme"
    pssm_path = inputdir + mat_id
    pssm = read_meme_file(pssm_path)
    w = m["w"]
    offset = m["loffset"]
    strand = m["strand"]
    pwm(pssm).render(fig, ax, type = "ic", xoffset = offset, xlim = (0 - padding, right + padding), rc = True if strand == "-" else False)
    ax.axvline(s, color = 'black', ls = '--')
    ax.axvline(e, color = 'black', ls = '--')
    axis_text = m["id2"] + "-" + m["name2"]
    ax.text(0.90, 0.5, axis_text, transform = ax.transAxes, ha = 'left', va = 'center', fontsize = "medium")
    [ax.spines[loc].set_color("none") for loc in ["top", "right"]]

plt.savefig(outfile + ".pdf")
plt.savefig(outfile + ".png")
