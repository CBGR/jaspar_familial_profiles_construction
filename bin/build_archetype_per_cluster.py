##################
## Load modules ##
##################

import sys
import os.path
import pandas as pd
import numpy as np
import scipy as sp
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import rcParams
rcParams['pdf.fonttype'] = 42
rcParams['svg.fonttype'] = 'none'
from genome_tools.plotting import pwm
import re

##################################
## Definition of some functions ##
##################################

# Function to read the input meme file
def read_meme_file(filepath):
	data = open(filepath).read()
	pos = 0
	while(1):
		rec_loc = data.find('\nMOTIF', pos)
		if rec_loc < 0:
			break
		nl = data.find('\n', rec_loc + 1)
		motif = data[rec_loc+6:nl].strip()
		mat_header_start = data.find('letter-probability', rec_loc)
		mat_header_end = data.find('\n', mat_header_start + 1)
		i = data.find(':', mat_header_start)
		kp = data[i+1:mat_header_end]
		r = re.compile(r"(\w+)=\s?([\w.\+]+)\s?")
		attribs = dict(r.findall(kp))
		ist = mat_header_end + 1
		iend=-1
		j = 0
		z = []
		while(j < int(attribs["w"])):
			iend = data.find('\n', ist)
			z.append(list(map(float, data[ist:iend].strip().split())))
			ist = iend + 1
			j += 1
		pwm = pd.DataFrame(list(map(np.ravel, z)))
		pwm = pwm.transpose()
		pwm = np.array(pwm)
		pos = ist
	return pwm

# Function to add zeros or cut a motif depending on where the motif is respective to the archetype
def get_archetypal_seq(pssm, m, s, e, right):
	offset = m["loffset"]
	strand = m["strand"]
	width = m["w"]

	if strand == "-":
		pssm = np.flip(pssm)

	if offset > 0:
		left_tail = np.zeros(offset)
	else:
		left_tail = np.array([])

	if offset + width < right:
		right_tail = np.zeros(right-(offset+width))
	else:
		right_tail = np.array([])

	arch_seq = []
	for nt in range(len(pssm)):
		full_seq_nt = np.insert(pssm[nt], 0, left_tail)
		full_seq_nt = np.insert(full_seq_nt, len(full_seq_nt), right_tail)

		arch_seq_nt = full_seq_nt[s:e]
		arch_seq.append(list(arch_seq_nt))

	return arch_seq

# Function to compute the archetypal matrix for a cluster
def compute_arch_per_cluster(arch_seqs, s, e):
	n_motifs = len(arch_seqs)
	for nt in range(4):
		pssms_nt = []
		for i in range(n_motifs):
			pssms_nt.append(arch_seqs[i][nt])
		if nt == 0:
			pssms_nt = np.asarray(pssms_nt)
			average_nt = pssms_nt.sum(axis = 0)/len(pssms_nt)
		else:
			pssms_nt = np.asarray(pssms_nt)
			av_int = pssms_nt.sum(axis = 0)/len(pssms_nt)
			average_nt = np.vstack([average_nt, av_int])
	return average_nt

######################
## Argument parsing ##
######################

cl_info_file = sys.argv[1]
cl_motifs_file = sys.argv[2]
outfile = sys.argv[3]
inputdir = sys.argv[4]

######################
## Read input files ##
######################

cluster_info = pd.read_csv(cl_info_file, delimiter = "\t", header = None)
cluster_info.columns = ["cl", "seed_motif", "roffset", "s", "e", "N"]

cl_motifs_info = pd.read_csv(cl_motifs_file, delimiter = "\t", header = 0)

#################################################
## Compute the archetype for the input cluster ##
#################################################

N = cl_motifs_info.shape[0]
right = np.max(cl_motifs_info["loffset"] + cl_motifs_info["w"])

s = cluster_info["s"][0]
e = cluster_info["e"][0]

arch_seqs = []

for i in range(N):

	m = cl_motifs_info.iloc[i,:]

	mat_id = m["id2"]
	# mat_id = mat_id[0]
	mat_id = mat_id + ".meme"

	pssm_path = inputdir + mat_id
	pssm = read_meme_file(pssm_path)
	w = m["w"]
	offset = m["loffset"]
	strand = m["strand"]
	arch_seqs.append(get_archetypal_seq(pssm, m, s, e, right))

fig = plt.figure()
ax = fig.add_subplot()
pwm(compute_arch_per_cluster(arch_seqs, s, e)).render(fig, ax, type = "ic", xlim = (0, e-s), rc = False)
ax.set_yticks([0.0, 0.5, 1.0, 1.5, 2.0])
ax.set_yticklabels([0.0, 0.5, 1.0, 1.5, 2.0])
for i in ax.spines:
	ax.spines[i].set_visible(True)
ax.set_ylabel("Bits")
plt.savefig(outfile + "_archetype.pdf")
plt.savefig(outfile + "_archetype.png")
