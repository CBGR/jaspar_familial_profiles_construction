import sys
import pandas as pd

## Define some functions for convenience

def read_cluster_info(filepath):

	names = {}
	widths = {}
	loffsets = {}
	roffsets = {}

	with open(filepath) as file:
		for line in file:

			(c, name, w, l, r, n) = line.strip().split('\t')

			names[c]=name.split("_")[0]
			widths[c]=int(w)
			loffsets[c]=int(l)
			roffsets[c]=int(r)

	return (loffsets, roffsets, widths, names)


def read_motif_info(filepath):

	loffsets = {}
	roffsets = {}
	widths = {}
	strands = {}
	cl = {}

	with open(filepath) as file:
		for line in file:

			(id, motif, w, s, l, r, c) = line.strip().split('\t')

			motif = motif.split("_")[-1]

			loffsets[motif]=int(l)
			roffsets[motif]=int(r)
			widths[motif]=int(w)
			strands[motif]=s
			cl[motif]=c

	return (loffsets, roffsets, strands, widths, cl)

## Argument parsing and reading input files

info_file=sys.argv[1]
motif_file=sys.argv[2]
TFBS_file=sys.argv[3]
class_annotations=sys.argv[4]

(clo, cro, cw, cn) = read_cluster_info(info_file)
(mlo, mro, ms, mw, mcl) = read_motif_info(motif_file)

annots = pd.read_csv(class_annotations, header = 0, delimiter = "\t")

# For each line in the UniBind TFBSs, extract the relevant information and process to get archetype binding site

TFBS = open(TFBS_file)
for line in TFBS:

	try:

		(chrom, start, end, motif, score, strand, tf)=line.strip().split('\t')
		start=int(start)
		end=int(end)

		cl=mcl[motif]
		l=mlo[motif]
		r=mro[motif]
		s=ms[motif]
		w=mw[motif]

		matrix_ID = motif.split(".")[0]
		cl_name = annots[annots["BASE_ID"] == matrix_ID]
		if cl_name.shape[0] == 0:
			cl_name = "Unknown"
		else:
			cl_name = cl_name.iloc[[0,]]
			cl_name = cl_name.values[0][-2]

		cl_w=cw[cl]
		cl_l=clo[cl]
		cl_r=cro[cl]

		cl_lo=cl_l
		cl_ro=cl_w-cl_r

		if strand==s:
			nstart=start-l+(cl_lo)
			nend=end+r-(cl_ro)
			nstrand="+"
		else:
			nstart=start-r+cl_ro
			nend=end+l-cl_lo
			nstrand="-"

		# bit of a hack for small chromosomes
		if nstart<0:
			continue



		print("%s\t%d\t%d\t%s:%s\t%s\t%s\t%d\t%d\t%s" % (chrom, nstart, nend, cl, cl_name, score, nstrand, nstart, nend, tf))

	except KeyError:
		pass
