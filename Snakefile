import os

configfile: "config.yaml"

DATA_DIR = config["DATA_DIR"]
RESULTS_DIR = config["RESULTS_DIR"]
TAXONS = config["TAXONS"]

rule all:
    input:
        expand(os.path.join(RESULTS_DIR, "{taxon}", "all_archetypes.pdf"), taxon = TAXONS)

rule convert_transfac_to_meme:
    """
    Converts the input Transfac motif file to meme and stores it in the results directory.
    """
    input:
        os.path.join(DATA_DIR, "motifs", "{taxon}", "motifs.tf")
    output:
        os.path.join(RESULTS_DIR, "{taxon}", "motifs", "motifs.meme")
    shell:
        """
        mkdir -p $(dirname {output});

        Rscript bin/convert_transfac_to_meme.R \
        -i {input} \
        -o {output};
        """

rule format_pairwise_comparison_file:
    """
    Format the pairwise comparison table and store it in the results directory.
    """
    input:
        os.path.join(DATA_DIR, "pairwise_comparisons", "{taxon}", "pairwise_compa.txt")
    output:
        os.path.join(RESULTS_DIR, "{taxon}", "pairwise_compa.txt")
    shell:
        """
        Rscript bin/reformat_pairwise_compa.R \
        -i {input} \
        -o {output};
        """

rule format_clusters_file:
    """
    Format the clusters file and store it in the results directory.
    """
    input:
        os.path.join(DATA_DIR, "clusters", "{taxon}", "clusters.txt")
    output:
        os.path.join(RESULTS_DIR, "{taxon}", "clusters.txt")
    shell:
        """
        Rscript bin/reformat_cluster_results.R \
        -i {input} \
        -o {output};
        """

checkpoint process_clusters:
    """
    Create two files (cluster-info.#.txt and cluster-motifs.#.txt) for each cluster,
    where # is the cluster number. The files contain information relevant for the
    alignment of the motifs.
    """
    input:
        clusters = os.path.join(RESULTS_DIR, "{taxon}", "clusters.txt"),
        motifs = os.path.join(RESULTS_DIR, "{taxon}", "motifs", "motifs.meme"),
        pairwise_comparisons = os.path.join(RESULTS_DIR, "{taxon}", "pairwise_compa.txt")
    output:
        directory(os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters"))
        # cluster_info = os.path.join(RESULTS_DIR, "cluster-info.{cl}.txt"),
        # cluster_motifs = os.path.join(RESULTS_DIR, "cluster-motifs.{cl}.txt")
    shell:
        """
        mkdir -p {output};

        python bin/process_cluster.py \
        {input.pairwise_comparisons} \
        {input.clusters} \
        {output} \
        {input.motifs};
        """

rule visualize_cluster:
    """
    For each cluster, use the files produced in the previous rule to align and
    plot all cluster member motifs.
    """
    input:
        cluster_info = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "cluster-info.{cl}.txt"),
        cluster_motifs = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "cluster-motifs.{cl}.txt")
    output:
        pdf = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}.pdf"),
        png = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}.png")
    params:
        motifs_dir = os.path.join(RESULTS_DIR, "{taxon}", "motifs")
    shell:
        """
        mkdir -p $(dirname {output.pdf});

        outfile={output.pdf};
        outfile=${{outfile%".pdf"}};

        python bin/viz_cluster.py \
        {input.cluster_info} \
        {input.cluster_motifs} \
        $outfile \
        {params.motifs_dir}/;
        """

rule build_archetype_per_cluster:
    """
    For each cluster, build and plot the archetype as the average information content
    per nucleotide and position.
    """
    input:
        cluster_info = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "cluster-info.{cl}.txt"),
        cluster_motifs = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "cluster-motifs.{cl}.txt"),
        pdf = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}.pdf"),
        png = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}.png")
    output:
        pdf = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}_archetype.pdf"),
        png = os.path.join(RESULTS_DIR, "{taxon}", "processed_clusters", "plots", "cluster{cl}_archetype.png")
    params:
        motifs_dir = os.path.join(RESULTS_DIR, "{taxon}", "motifs")
    shell:
        """
        mkdir -p $(dirname {input.pdf});
        outfile={output.pdf};
        outfile=${{outfile%%"_archetype.pdf"}};

        python bin/build_archetype_per_cluster.py \
        {input.cluster_info} \
        {input.cluster_motifs} \
        $outfile \
        {params.motifs_dir}/;
        """

def get_clusters_output(wildcards):
    checkpoint_output = checkpoints.process_clusters.get(**wildcards).output[0]
    new_files = expand(os.path.join(checkpoint_output, "plots", "cluster{cl}_archetype.pdf"), cl = glob_wildcards(os.path.join(checkpoint_output, "cluster-info.{cl}.txt")).cl)
    return new_files

rule collect:
    """
    Collect all archetypes and put them in one PDF file for inspection.
    """
    input:
        get_clusters_output
    output:
        os.path.join(RESULTS_DIR, "{taxon}", "all_archetypes.pdf")
    shell:
        """
        pdfunite {input} {output};
        """
